//
//  ViewController.swift
//  Clima
//
//  Created by Angela Yu on 01/09/2019.
//  Copyright © 2019 App Brewery. All rights reserved.
//

import SwiftUI
import CoreLocation

class WeatherViewController: UIViewController {
    
    @IBOutlet weak var conditionImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var unitLabel: UILabel!
    
    var weatherManager = WeatherManager()
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        searchTextField.delegate = self
        weatherManager.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
        self.hideKeyboardWhenTappedAround()
        
    }
    
    @IBAction func changeUnit(_ sender: UITapGestureRecognizer) {
        
        if unitLabel.text != "F" {
            unitLabel.text = "F"
            temperatureLabel.text = String(format: "%.1f", ((temperatureLabel.text! as NSString).doubleValue * 1.8) + 32)
            
        } else {
            unitLabel.text = "C"
            temperatureLabel.text = String(format: "%.1f", ((temperatureLabel.text! as NSString).doubleValue - 32) / 1.8)
            
            
        }
    }
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        searchTextField.placeholder = "Type something..."
        searchTextField.endEditing(true)
    }
    
    //    func changesUnitOnTap() {
    //        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: unitLabel, action: #selector(self.changeUnitTap))
    //        tap.cancelsTouchesInView = false
    //        unitLabel.addGestureRecognizer(tap)
    //    }
    //
    //    @objc func changeUnitTap() {
    //        print("tap")
    //        if unitLabel.text != "F" {
    //            unitLabel.text = "F"
    //                        temperatureLabel.text = weather.tempString * 1.8 + 32
    //        } else {
    //            unitLabel.text = "C"
    //                       temperatureLabel.text = weather.tempString * 1.8 + 32
    //        }
    //    }
    
}
//MARK: - CLLocationManagerDelegate
extension WeatherViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            locationManager.stopUpdatingLocation()
            let lat = location.coordinate.latitude
            let lon = location.coordinate.longitude
            weatherManager.fetchWeather(lat, lon)
            print(lat, lon)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    @IBAction func locationPressed(_ sender: UIButton) {
        locationManager.requestLocation()
    }
}
//MARK: - UITextFieldDelegate
extension WeatherViewController: UITextFieldDelegate {
    
    @IBAction func searchPressed(_ sender: UIButton) {
        
        print(searchTextField.text!)
        if let city = searchTextField.text {
            weatherManager.fetchWeather(city)
        }
        searchTextField.text = ""
        searchTextField.endEditing(true)
        searchTextField.placeholder = "Search"
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print(searchTextField.text!)
        if let city = searchTextField.text {
            weatherManager.fetchWeather(city)
        }
        searchTextField.text = ""
        searchTextField.endEditing(true)
        searchTextField.placeholder = "Search"
        return true
    }
}

//MARK: - WeatherManagerDelegate

extension WeatherViewController: WeatherManagerDelegate {
    
    func didUpdateWeather(_ weather: WeatherModel) {
        temperatureLabel.text = weather.tempString
        unitLabel.text = "F"
        cityLabel.text = weather.cityName
        print(weather.conditionName)
        conditionImageView.image = UIImage(systemName: weather.conditionName)
        
    }
    
    func didFailWithError(_ error: Error) {
        DispatchQueue.main.async {
            self.searchTextField.placeholder = "Error: Try Again"
            print(error)
        }
    }
}

struct WeatherViewController_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}

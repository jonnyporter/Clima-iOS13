//
//  WeatherManager.swift
//  Clima
//
//  Created by Jonathan Porter on 8/20/20.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import Foundation


protocol WeatherManagerDelegate {
    func didUpdateWeather(_ weather: WeatherModel)
    func didFailWithError(_ error: Error)
}

struct WeatherManager {
    
    var delegate: WeatherManagerDelegate?
    
    let weatherURLF = "https://api.openweathermap.org/data/2.5/weather?appid=e8ce9247dde7f2f20e21f1f336b5ac7b&units=imperial"
    let weatherURLC = "https://api.openweathermap.org/data/2.5/weather?appid=e8ce9247dde7f2f20e21f1f336b5ac7b&units=metric"
    
    func fetchWeather(_ cityName: String) {
        let urlString = "\(weatherURLF)&q=\(cityName)"
        print(urlString)
        performRequest(urlString)
    }
    
    func fetchWeather(_ latitude: Double, _ longitude: Double) {
        let lat = String(format: "%.2f", latitude)
        let lon = String(format: "%.2f", longitude)
        let urlString = "\(weatherURLF)&lat=\(lat)&lon=\(lon)"
        print(urlString)
        performRequest(urlString)
    }
    
    
    func performRequest(_ urlString: String) {
        if let url = URL(string: urlString) {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url){ (data, response, error) in
                if error != nil {
                    self.delegate?.didFailWithError(error!)
                    return
                }
                
                if let safeData = data {
                    DispatchQueue.main.async {
                        if let weather = self.parseJSON(weatherData: safeData) {
                            self.delegate?.didUpdateWeather(weather)
                        }
                    }
                    
                }
            }
            task.resume()
        }
    }
    func parseJSON(weatherData: Data) -> WeatherModel? {
        let decoder = JSONDecoder()
        do {
            let decodable = try decoder.decode(WeatherData.self, from: weatherData)
            let name = decodable.name
            let temp = decodable.main.temp
            let id = decodable.weather[0].id
            
            let weather = WeatherModel(cityName: name, temp: temp, weatherID: id)
            return weather
        } catch {
            delegate?.didFailWithError(error)
            return nil
        }
    }
}
